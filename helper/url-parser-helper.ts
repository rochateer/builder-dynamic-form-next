export const urlParserHelper = (host : string | null, full_url : string | null) => {
    if(!full_url) return null
    let pageUrl = full_url
    if(host){
        const splitUrl = full_url.split(host)
        pageUrl = splitUrl[1]
    }
    return pageUrl
}