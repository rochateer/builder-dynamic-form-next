"use client"

import { menu_schema_type } from '@/type-schema/common/menu-type'
import Link from 'next/link'
import { usePathname } from 'next/navigation'
import React from 'react'

function HeaderMenuLink(props : menu_schema_type) {
  const pathname = usePathname()
  const mainPathname = '/'+pathname.split('/')[1]
  return (
    <Link
      href={props.link}
      className={`text-${mainPathname === props.link ? '' : 'muted-'}foreground transition-colors hover:text-foreground`}
    >
      {props.label}
    </Link>
  )
}

export default HeaderMenuLink