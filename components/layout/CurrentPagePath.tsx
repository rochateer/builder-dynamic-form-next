"use client"

import { usePathname } from 'next/navigation'
import React from 'react'

function CurrentPagePath({children} : {children : React.ReactNode}) {
  const pathname = usePathname()
  return (
    <>
      {children}
    </>
  )
}

export default CurrentPagePath