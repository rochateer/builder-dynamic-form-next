import { AuthOptions, NextAuthOptions } from "next-auth";
import GoogleProvider from "next-auth/providers/google";

console.log("process.env.NEXT_AUTH_SECRET_DYFORM", process.env.NEXT_AUTH_SECRET_DYFORM)
  
export const authOptions : NextAuthOptions = {
  providers : [
    GoogleProvider(
      {
        clientId : process.env.GOOGLE_CLIENT_ID_DYFORM as string,
        clientSecret : process.env.GOOGLE_CLIENT_SECRET_DYFORM as string,
        authorization: {
          params: {
            prompt: "consent",
            access_type: "offline",
            response_type: "code"
          }
        }
      }
    )
  ],
  session: {
    strategy: "jwt",
  },
  secret: process.env.NEXT_AUTH_SECRET_DYFORM
}