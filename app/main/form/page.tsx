import { Button } from '@/components/ui/button'
import { Card, CardContent, CardHeader, CardTitle } from '@/components/ui/card'
import Link from 'next/link'
import React from 'react'

function Form() {
  return (
    <>
      <div className="grid gap-4 md:gap-8 lg:grid-cols-2 xl:grid-cols-3">
        <Link href={'/form/create'}>
          <Button>Create Form</Button>
        </Link>
      </div>
      <div className="grid gap-4 md:gap-8 lg:grid-cols-2 xl:grid-cols-3">
        <Card>
          <CardHeader>
            <CardTitle>Form Title 1</CardTitle>
          </CardHeader>
          <CardContent>Form detail</CardContent>
        </Card>
        <Card>
          <CardHeader>
            <CardTitle>Form Title 2</CardTitle>
          </CardHeader>
          <CardContent>Form detail</CardContent>
        </Card>
      </div>
    </>
  )
}

export default Form