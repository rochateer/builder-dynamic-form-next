"use client"

import { Card, CardContent, CardHeader, CardTitle } from '@/components/ui/card'
import { Input } from '@/components/ui/input'
import React from 'react'

function FormCreation() {
  return (
    <>
      <div className="grid place-items-center">
        <Card className="w-7/12">
          <CardHeader className="place-items-center">
            <CardTitle>
              <Input type="text" placeholder="Form Title" className="text-xl text-center"/>
            </CardTitle>
          </CardHeader>
          <CardContent>
            
          </CardContent>
        </Card>
      </div>
    </>
  )
}

export default FormCreation