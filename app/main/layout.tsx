// #F5F7F8

"use client"

import Header from "@/components/layout/Header"
import { usePathname } from 'next/navigation'

export default function MainLayout({children} : {children : React.ReactNode}) {

  const pathname = usePathname()
  const mainPathname = '/'+pathname.split('/')[1]

  return (
    <div className="flex min-h-screen w-full flex-col">
      <Header mainPathname={mainPathname}/>
      <main className="flex flex-1 flex-col gap-4 p-4 md:gap-8 md:p-8">
        {children}
      </main>
    </div>
  )
}
